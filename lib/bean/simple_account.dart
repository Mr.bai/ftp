class SimpleAccount {

  String accountId = "";

  String userName = "";

  String fullName = "";

  String gender = "";

  String gradelevel = "";

  String deptid = "";

  String img = "";

  String cardNo = "";

  String time = "";

  String fee = "";

  String mealType = "";

  String position = "";

  SimpleAccount({
    accountId,
    userName,
    fullName,
    gender,
    gradelevel,
    deptid,
    img,
    cardNo,
    time,
    fee,
    mealType,
    position
  });

  SimpleAccount.fromJson (dynamic json) {
    accountId = json["accountId"] ?? null;
    userName = json["userName"] ?? null;
    fullName = json["fullName"] ?? null;
    gender = json["gender"] ?? null;
    gradelevel = json["gradelevel"] ?? null;
    deptid = json["deptid"] ?? null;
    img = json["img"] ?? null;
    cardNo = json["cardNo"] ?? null;
    time = json["time"] ?? null;
    fee = json["fee"] ?? null;
    mealType = json["mealType"] ?? null;
    position = json["position"] ?? "1号餐厅";
  }

  Map<String, dynamic> toJson () {
    var map = <String, dynamic>{};

    map["accountId"] = accountId;
    map["userName"] = userName;
    map["fullName"] = fullName;
    map["gender"] = gender;
    map["gradelevel"] = gradelevel;
    map["deptid"] = deptid;
    map["img"] = img;
    map["cardNo"] = cardNo;
    map["time"] = time;
    map["fee"] = fee;
    map["mealType"] = mealType;
    map["position"] = position;
    return map;
  }

  @override
  String toString() {
    return 'SimpleAccount{accountId: $accountId, userName: $userName, fullName: $fullName, gender: $gender, gradelevel: $gradelevel, deptid: $deptid, img: $img, cardNo: $cardNo, time: $time, fee: $fee, mealType: $mealType, position: $position}';
  }
}