import 'package:flutter_pos_printer_platform/flutter_pos_printer_platform.dart';

class PrinterDeviceInfo {
  int? id;
  String? deviceName;
  String? address;
  String? port;
  String? vendorId;
  String? productId;
  bool? isBle;

  PrinterType typePrinter;
  bool? state;

  PrinterDeviceInfo({this.deviceName,
    this.address,
    this.port,
    this.state,
    this.vendorId,
    this.productId,
    this.typePrinter = PrinterType.usb,
    this.isBle = false});
}