# FTP（校园小票打印App）
FTP : Flutter Ticket Printer

一个基于Flutter开发运行在Windows平台的校园小票打印工具

---

> Windows系统

```
Windows 10
```

---

> 当前Flutter 版本

```
Flutter 3.10.0 • channel stable • https://github.com/flutter/flutter.git
Framework • revision 84a1e904f4 (4 weeks ago) • 2023-05-09 07:41:44 -0700
Engine • revision d44b5a94c9
Tools • Dart 3.0.0 • DevTools 2.23.1
```

---

#### 校园小票打印App 界面

| . | . | . |
|------|------------|------------|
| ![](./screenshot/1.png)|   ![](./screenshot/1.png)|      ![](./screenshot/3.jpg)     |
| ![](./screenshot/4.jpg)|    |           |

---